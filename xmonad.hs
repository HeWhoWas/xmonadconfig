import XMonad
import XMonad.Config.Xfce
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.SetWMName
import XMonad.Hooks.UrgencyHook
import XMonad.Layout.Fullscreen
import XMonad.Layout.NoBorders
import XMonad.Layout.IM
import XMonad.Layout.Grid
import XMonad.Layout.PerWorkspace
import qualified XMonad.StackSet as W
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig(additionalKeys)
import Control.Monad (liftM2)
import Data.List
import Data.Ratio ((%))
import System.IO

main :: IO ()
main = do
	--topbar1 <- spawnPipe "/usr/bin/xmobar -x 1 /home/ben/.xmobarrc"
	topbar2 <- spawnPipe "/usr/bin/xmobar -x 1 /home/ben/.xmobarrc"
        xmonad $ xfceConfig
        	{ startupHook = setWMName "LG3D",
		manageHook = manageDocks <+> myManageHook <+> manageHook xfceConfig
		, layoutHook = myLayout 
		, logHook = myLogHook topbar2
		, workspaces = myWorkspaces
		} 
		 `additionalKeys`
		[ ((mod4Mask .|. shiftMask, xK_z), spawn "gnome-screensaver-command --lock") --mod4mask is the windows key
		, ((0, xK_Print), spawn "gnome-screenshot")
		, ((mod1Mask .|. shiftMask, xK_i), spawn "firefox")
	    	, ((mod1Mask .|. shiftMask, xK_e), spawn "thunar")
		, ((mod1Mask, xK_F4 ), kill) -- %! Close the focused window
		, ((mod1Mask, xK_p), spawn "dmenu_run -b")
		, ((mod1Mask, xK_F2), spawn "dmenu_run -b")
		]

colorBlack	= "#020202"        -- RGB hex. color definitions
colorBlackAlt   = "#1c1c1c"
colorGray       = "#444444"        -- also fits    #444
colorGrayAlt    = "#222222"        --        #222
colorWhite      = "#dddddd"        --        #ddd
colorWhiteAlt   = "#aaaaaa"        --        #aaa
colorMagenta    = "#8e82a2"
colorBlue       = "#3399ff"        --        #39f
colorRed        = "#d74b73"
colorGreen      = "#99cc66"        --        #9c6



myLogHook topbar2 = dynamicLogWithPP topBarPP { ppOutput = hPutStrLn topbar2 }
--myLogHook topbar1 = dynamicLogWithPP topBarPP { ppOutput = hPutStrLn topbar1 }

topBarPP = xmobarPP { 
                      ppTitle = xmobarColor "green" "" . shorten 120
                    , ppHiddenNoWindows = xmobarColor colorGray colorGrayAlt
                    }

myLayout = avoidStruts 	$ onWorkspace "5:consoles" consoleLayout
			$ onWorkspace "3:chat" chatLayout
			$ defaultLayouts
	where
	tall	=	Tall	1	0.02	0.5
	full	=	noBorders(fullscreenFull Full)
	defaultLayouts = tall ||| Mirror tall ||| full
	consoleLayout = withIM (1%7) (ClassName "Toplevel") Grid
	chatLayout = withIM (1%7) (Title "Buddy List") Grid

myWorkspaces = ["1:main","2:web","3:chat","4:develop","5:consoles","6:windows","7","8","9:pass"]

myManageHook = composeAll . concat $
    [ 
      [ className   =? c --> doIgnore | c <- ignoredApps]
    , [ className   =? c --> doFloat           | c <- myFloats]
    , [ className   =? c --> doF (W.shift "2:web") | c <- webApps]
    , [ className   =? c --> doF (W.shift "3:chat") | c <- chatApps]
    , [ className   =? c --> doF (W.shift "4:develop") | c <- developApps]
    , [ className   =? c --> doF (W.shift "5:consoles") | c <- consoleApps]
    , [ title	    =? c --> doF (W.shift "5:consoles") | c <- consoleAppsByName]
    , [ className   =? c --> doF (W.shift "6:windows") | c <- windowsApps]
    , [ className   =? c --> doF (W.shift "9:pass") | c <- passApps]
    ]
	where 	ignoredApps 	 = ["trayer"]	  
		myFloats      	 = []
		webApps        	 = ["Firefox", "Opera", "Chromium-browser"]
		chatApps      	 = ["Pidgin", "Skype"]      
		developApps      = ["SmartGit/Hg", "Eclipse", "jetbrains-idea-ce", "jetbrains-phpstorm", "Rapidsvn", "jetbrains-pycharm"]      
		consoleApps      = ["Xfce4-terminal", "TopLevel", "X-terminal-emulator"]      
		consoleAppsByName = ["secpanel - Agent active (external)"]      
		passApps	 = ["KeePass2"]      
		windowsApps	 = []
		
